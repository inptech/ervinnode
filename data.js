const products = [
        {
            id: 1,
            name: 'albany sofa',
            price: 39.95,
            desc: `I'm baby direct trade farm-to-table hell of, YOLO readymade raw denim venmo whatever organic gluten-free kitsch schlitz irony af flexitarian.`,
        },
    
        {
            id: 2,
            name: 'black sofa',
            price: 19.95,
            desc: `I'm baby direct trade farm-to-table hell of, YOLO readymade raw denim venmo whatever organic gluten-free kitsch schlitz irony af flexitarian.`,
        }
    ]
    
    const people = [
        { id: 1, name: 'Ervin' },
        { id: 2, name: 'Elzin' },
        { id: 3, name: 'Ahmed' },
        { id: 4, name: 'Kennan' },
        { id: 5, name: 'Elmedin' },
    ]
    
    module.exports = { products, people }
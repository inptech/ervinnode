const EventEmitter = require('events');
const eventEmitter = new EventEmitter();

eventEmitter.on('tutorial',(num1, num2)=>{
    console.log(num1 + num2);
});

eventEmitter.emit('tutorial',1,2);

class Person extends EventEmitter {
    constructor(name){
        super();
        this._name = name;
    }
    get name() {
        return this._name;
    }
}

let angelina = new Person('Angelina');
angelina.on('name',()=>{
    console.log('My name is' + " " + angelina.name);
});
let ervin = new Person('Ervin');
ervin.on('name',()=>{
    console.log('My name is' + " " + ervin.name);
});

angelina.emit('name');
ervin.emit('name');